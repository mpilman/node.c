Node.C++
========

Node.C++ is a framework which allows a user to easily write his own web server. At the moment, it contains the following components:

- A tcp server and http server library.
- A json library.
- A easy to use MySQL database client library.

Node.C++ makes heavy use of the new C++11 features. Therefore a very recent compiler is needed to use it. At the moment it is only tested on gcc 4.6 on Linux and Ubuntu.

= Building it =

Node.C++ needs CMake (http://www.cmake.org) as a build tool. Also, since gcc 4.6 does not implement the whole most recent STL (regular expressions on all platforms and threads on Mac OS X) you will also need boost to build the server library.

If you want to build the mysql library, you also need to have an installed version of the mysql client libraries.

