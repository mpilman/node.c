#pragma once
#include <vector>
#include <string>

class redis_context;

class web_page
{
public:
    web_page(redis_context &context);
    std::vector<std::string> get_page_titles();
private:
    redis_context &_context;
};
