/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nodecpp/json/json.h>
#include <nodecpp/server/http_server.h>
#include <iostream>

using namespace nodecpp::json;
using namespace nodecpp::server;

typedef std::string s;

int main()
{
    http_server server(8080);
    server["/.*"] = [](const http_request& req, std::iostream& stream)
    {
        http_response resp;
        resp["Content-Type"] = "text/html";
        stream << resp;
        stream << "<html><head><title>Hello World</title></head><body><h1>It Works!</h1></body></html>";
    };
    server["/simplejson"] = [](const http_request& req, std::iostream& stream)
    {
        http_response resp;
        resp["Content-Type"] = "application/json";
        json j =
        {
            s("a") << "b",
            s("hello") << json(
            {
                s("foo") << "bar",
                s("list") << json({1,2,3,4})
            })
        };
        stream << resp;
        stream << j;
    };
    server.main_loop();
    return 0;
}
