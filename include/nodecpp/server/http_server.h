/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include <functional>
#include <iostream>
#include <map>

namespace nodecppimpl { namespace server {
class http_request;
class http_response;
class http_server;
class file_handler;
}} //impl-namespaces

namespace nodecpp { namespace server {

class http_exception : public std::exception {
public:
    http_exception(const std::string& msg) : _msg(msg) {}
    http_exception(std::string&& msg) : _msg(msg) {}
    http_exception(const http_exception& ex) : _msg(ex._msg) {}
    http_exception& operator =(const http_exception& ex) { _msg = ex._msg; return *this; }
    virtual ~http_exception() throw() {}
    virtual const char *what() const throw() { return _msg.c_str(); }
protected:
    std::string _msg;
};

class file_not_found : public http_exception
{
public:
    file_not_found(const std::string& msg) : http_exception(msg) {}
    file_not_found(std::string&& msg) : http_exception(msg) {}
    file_not_found(const file_not_found& ex) : http_exception(ex._msg) {}
    file_not_found& operator =(const file_not_found& ex) { _msg = ex._msg; return *this; }
    virtual ~file_not_found() throw() {}
};

class http_request {
    friend class nodecppimpl::server::http_server;
    http_request() = delete;
    http_request(const http_request&) = delete;
    http_request& operator= (const http_request&) = delete;
private:
    nodecppimpl::server::http_request *_impl;
private:
    http_request(nodecppimpl::server::http_request* impl);
public:
    std::string operator[] (const std::string& key) const;
    const std::string &method() const;
    const std::string &path() const;
};

class http_response {
private:
    int _status;
    std::string _msg;
    std::map<std::string, std::string> _headers;
public:
    http_response(int status, const std::string& message) : _status(status), _msg(message) {}
    http_response(int status, std::string&& message) : _status(status), _msg(message) {}
    http_response() : _status(200), _msg("OK") {}
    std::string& operator [](const std::string& key)
    {
        return _headers[key];
    }
    void serialize(std::ostream &s) const;
};

class http_server {
    http_server(const http_server&) = delete;
    http_server& operator= (const http_server&) = delete;
private:
    nodecppimpl::server::http_server *_impl;
public:
    http_server(short port);
    http_server(short port, std::function<void (const http_request&, std::iostream& stream)> def_handler);
    http_server(http_server&& other);
    ~http_server();
    http_server &operator =(const std::function<void (const http_request&, std::iostream& stream)>& def_handler);
    std::function<void (const http_request&, std::iostream& stream)>& operator [](const std::string& regex);
    void main_loop();
};

class file_handler {
private:
    nodecppimpl::server::file_handler *_impl;
public:
    file_handler(const std::string &root_dir, const std::string &pattern);
    file_handler(file_handler &&other);
    file_handler(const file_handler &other);
    ~file_handler();
    file_handler &operator =(const file_handler &other);
    file_handler &operator =(file_handler &&other);
    std::string &operator [](const std::string &suffix);
    void operator ()(const http_request &req, std::ostream &s);
};

}} //namespaces

namespace std {
ostream& operator <<(ostream& stream, const nodecpp::server::http_response& response);
}
