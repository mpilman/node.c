# Copyright 2011 Markus Pilman <markus@pilman.ch>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
set(SRCS json.hh
         json.cc
         json_parser.cc
         json_deleter.hh
         unmarshaller.hh
         json_serializer.cc
         ${CMAKE_SOURCE_DIR}/include/nodecpp/json/json.h
)

add_library(json SHARED ${SRCS})
target_link_libraries(json ${Boost_LIBRARIES})
