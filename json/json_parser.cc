/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "json.hh"
#include <iostream>
#include <sstream>
#include <memory>
#include <cmath>
#include <cctype>

namespace nodecppimpl { namespace json {

static inline int get_digit(int c) {
    switch (c) {
    case '0':
        return 0;
    case '1':
        return 1;
    case '2':
        return 2;
    case '3':
        return 3;
    case '4':
        return 4;
    case '5':
        return 5;
    case '6':
        return 6;
    case '7':
        return 7;
    case '8':
        return 8;
    case '9':
        return 9;
    }
}

static inline std::string get_word(std::istream& in)
{
    std::stringstream ss;
    char c;
    while (in.good()) {
        in.get(c);
        if (isalnum(c)) {
            ss << c;
        } else {
            in.unget();
            return ss.str();
        }
    }
    return ss.str();
}

static inline std::string get_unicode_point(std::istream& in)
{
    char res[2] = { 0, 0 };
    char c;
    for (int i = 0; i < 4; ++i) {
        in.get(c);
        if (in.eof()) {
            throw nodecpp::json::json_exception("Unexpected EOF");
        }
        switch (c) {
        case '0':
            res[i < 2 ? 0 : 1] = 0 << i % 2 == 0 ? 0 : 4;
            break;
        case '1':
            res[i < 2 ? 0 : 1] = 1 << i % 2 == 0 ? 0 : 4;
            break;
        case '2':
            res[i < 2 ? 0 : 1] = 2 << i % 2 == 0 ? 0 : 4;
            break;
        case '3':
            res[i < 2 ? 0 : 1] = 3 << i % 2 == 0 ? 0 : 4;
            break;
        case '4':
            res[i < 2 ? 0 : 1] = 4 << i % 2 == 0 ? 0 : 4;
            break;
        case '5':
            res[i < 2 ? 0 : 1] = 5 << i % 2 == 0 ? 0 : 4;
            break;
        case '6':
            res[i < 2 ? 0 : 1] = 6 << i % 2 == 0 ? 0 : 4;
            break;
        case '7':
            res[i < 2 ? 0 : 1] = 7 << i % 2 == 0 ? 0 : 4;
            break;
        case '8':
            res[i < 2 ? 0 : 1] = 8 << i % 2 == 0 ? 0 : 4;
            break;
        case '9':
            res[i < 2 ? 0 : 1] = 9 << i % 2 == 0 ? 0 : 4;
            break;
        case 'a':
        case 'A':
            res[i < 2 ? 0 : 1] = 10 << i % 2 == 0 ? 0 : 4;
            break;
        case 'b':
        case 'B':
            res[i < 2 ? 0 : 1] = 11 << i % 2 == 0 ? 0 : 4;
            break;
        case 'c':
        case 'C':
            res[i < 2 ? 0 : 1] = 12 << i % 2 == 0 ? 0 : 4;
            break;
        case 'd':
        case 'D':
            res[i < 2 ? 0 : 1] = 13 << i % 2 == 0 ? 0 : 4;
            break;
        case 'e':
        case 'E':
            res[i < 2 ? 0 : 1] = 14 << i % 2 == 0 ? 0 : 4;
            break;
        case 'f':
        case 'F':
            res[i < 2 ? 0 : 1] = 15 << i % 2 == 0 ? 0 : 4;
            break;
        default:
            throw nodecpp::json::json_exception("Unexpected '" + std::to_string(c) + " expected [a-fA-F0-9]");
        }
    }
    return std::string(res, 2);
}

std::string json::parse_position::to_string()
{
    return std::to_string(line) + ":" + std::to_string(column);
}

std::string json::get_escaped(std::istream &in, parse_position &p)
{
    ++p.column;
    char c;
    in.get(c);
    if (in.eof())
        throw nodecpp::json::json_exception("Unexpected EOF at " + p.to_string());
    switch (c) {
    case '"':
        return "\"";
    case '\\':
        return "\\";
    case '/':
        return "/";
    case 'b':
        return "\b";
    case 'f':
        return "\f";
    case 'n':
        return "\n";
    case 'r':
        return "\r";
    case 't':
        return "\t";
    case 'u':
        p.column += 4;
        return get_unicode_point(in);
    default:
        throw nodecpp::json::json_exception("Unexpected '" + std::to_string(c) + "', expected escape characted ('\\', '/', 'b', 'f', 'n', 'r', 't', or uXXXX)");
    }
}

std::string json::parse_string(std::istream &in, parse_position &p)
{
    char c;
    in.get(c);
    if (c != '"') {
        throw nodecpp::json::json_exception("Unexpected '" + std::to_string(c) + "', expected \" at " + p.to_string());
    } else if (in.eof()) {
        throw nodecpp::json::json_exception("Unexpected EOF at " + p.to_string());
    }
    ++p.column;
    std::stringstream ss;
    while (in.good()) {
        in.get(c);
        switch (c) {
        case '"':
            ++p.column;
            return ss.str();
        case '\\':
            ++p.column;
            ss << get_escaped(in, p);
            break;
        default:
            ss << c;
        }
    }
    throw nodecpp::json::json_exception("Unexpected EOF at " + p.to_string());
}

json_value *json::parse_number(std::istream &in, parse_position &p)
{
    bool is_double = false;
    double double_val = 0.0;
    long long int_val = 0;
    bool negative = false;
    double exp = 1.0;
    bool exp_first = true;
    bool in_exponent = false;
    int c = in.get();
    if (c == '-') {
        negative = true;
        c = in.get();
    }
    int_val = get_digit(c);
    if (int_val == 0) {
        return new json_int(int_val);
    }
    if (negative)
        int_val *= -1;
    while (in.good()) {
        c = in.get();
        switch (c) {
        case '-':
            if (!in_exponent && exp_first)
                throw nodecpp::json::json_exception("Unexpected character '-' in number but not before exponent and not as prefix at " + p.to_string());
            exp_first = false;
            exp = -1.0;
            break;
        case '+':
            if (!in_exponent && exp_first)
                throw nodecpp::json::json_exception("Unexpected character '+' in number but not before exponent at " + p.to_string());
            exp_first = false;
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            ++p.column;
            if (!is_double) {
                int_val *= 10;
                int_val += get_digit(c);
            } else if (in_exponent) {
                if (exp_first) {
                    exp *= static_cast<double>(get_digit(c));
                } else {
                    exp *= 10.0;
                    exp += static_cast<double>(get_digit(c));
                }
            } else {
                exp *= 10.0;
                double_val += static_cast<double>(get_digit(c))/exp;
                exp_first = false;
            }
            break;
        case '.':
            ++p.column;
            is_double = true;
            double_val = int_val;
            break;
        case 'e':
        case 'E':
            ++p.column;
            in_exponent = true;
            exp = 1.0;
            break;
        default:
            in.unget();
            if (is_double && in_exponent) {
                return new json_double(pow(double_val, exp));
            } else if (is_double) {
                return new json_double(double_val);
            } else {
                return new json_int(int_val);
            }
        }
    }
}

json_value *json::parse_value(std::istream &in, parse_position &p)
{
    int c;
    std::string w;
    std::unique_ptr<json> js;
    while (in.good()) {
        c = in.get();
        switch (c) {
        case '-':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            in.unget();
            return parse_number(in, p);
        case 't':
        case 'f':
        case 'n':
            in.unget();
            w = get_word(in);
            if (w == "true") {
                p.column += 4;
                return new json_bool(true);
            } else if (w == "false") {
                p.column += 5;
                return new json_bool(false);
            } else if (w == "null") {
                p.column += 4;
                return new json_null();
            }
            break;
        case '[':
            ++p.column;
            js.reset(parse_array(in, p));
            return new json_array(js->_array);
        case '{':
            ++p.column;
            js.reset(parse_object(in, p));
            return new json_json(*js.get());
        case '"':
            in.unget();
            return new json_string(parse_string(in, p));
        default:
            throw nodecpp::json::json_exception("Unexpected character '" +
                                                std::to_string(static_cast<char>(c)) +
                                                "' for the first character of a value at " + p.to_string());
        }
    }
}

json *json::parse_array(std::istream &in, parse_position &p)
{
    std::unique_ptr<json> res(new json());
    res->_is_array = true;
    bool expect_separator = false;
    int c;
    while (in.good()) {
        c = in.get();
        switch (c) {
        case ' ':
        case '\t':
            ++p.column;
            break;
        case '\n':
        case '\r':
            ++p.line;
            break;
        case ']':
            ++p.column;
            return res.release();
        case ',':
            if (!expect_separator)
                throw nodecpp::json::json_exception("Did not expect a separator at " + p.to_string());
            expect_separator = false;
            break;
        default:
            in.unget();
            if (expect_separator)
                throw nodecpp::json::json_exception("Expected a separator at " + p.to_string());
            res->_array.push_back(nodecpp::json::json_value(parse_value(in, p)));
            expect_separator = true;
        }
    }
    throw nodecpp::json::json_exception("Expected ']' before EOF at " + p.to_string());
}

json *json::parse_object(std::istream &in, parse_position &p)
{
    std::unique_ptr<json> res(new json());
    bool expect_separator = false;
    bool expect_value = false;
    bool expect_name_separator = false;
    std::string last_key;
    char c;
    while (in.good()) {
        in.get(c);
        switch (c) {
        case ' ':
        case '\t':
            ++p.column;
            break;
        case '\n':
        case '\r':
            ++p.line;
            break;
        case '"':
            if (expect_separator || expect_name_separator)
                throw nodecpp::json::json_exception("Expected a separator at " + p.to_string());
            if (expect_value) {
                in.unget();
                res->_pairs[last_key] = parse_value(in, p);
                expect_separator = true;
                expect_value = false;
            } else {
                in.unget();
                last_key = parse_string(in, p);
                expect_name_separator = true;
            }
            break;
        case ':':
            if (!expect_name_separator || expect_separator || expect_value)
                throw nodecpp::json::json_exception("Did not expect ':' at " + p.to_string());
            expect_value = true;
            expect_name_separator = false;
            break;
        case ',':
            if (expect_name_separator || !expect_separator || expect_value)
                throw nodecpp::json::json_exception("Did not expect ',' at " + p.to_string());
            expect_separator = false;
            break;
        case '}':
            if (expect_value || expect_name_separator)
                throw nodecpp::json::json_exception("Did not expect '}' here: " + p.to_string());
            return res.release();
        default:
            if (!expect_value)
                throw nodecpp::json::json_exception("Did not expect value here: " + p.to_string());
            in.unget();
            res->_pairs[last_key] = parse_value(in, p);
            expect_separator = true;
            expect_value = false;
        }
    }
}

json *json::parse(std::istream &in)
{
    parse_position p = {0, 0};
    int c;
    while (in.good()) {
        c = in.get();
        switch (c) {
        case ' ':
        case '\t':
            ++p.column;
            break;
        case '\n':
        case '\r':
            ++p.line;
            break;
        case '[':
            ++p.column;
            return parse_array(in, p);
        case '{':
            ++p.column;
            return parse_object(in, p);
        default:
            throw nodecpp::json::json_exception("Unexpected character '" + std::to_string(static_cast<char>(c)) + "'. Excpected '{' or ']' at ");
        }
    }
}

}}
