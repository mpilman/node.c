/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "socket.h"
#include <functional>
#include <iostream>

namespace nodecppimpl {
namespace server {

class socket_streambuf : public std::streambuf {
public:
    socket_streambuf(tcp_socket& aSocket);
    virtual ~socket_streambuf();
protected:
    virtual int sync();
    virtual int underflow();
    virtual int overflow(int c = EOF);
private:
    tcp_socket& _socket;
    char_type* _input_buffer;
    char_type* _output_buffer;
};

class tcp_server {
private:
    short _port;
    std::function<void(std::iostream&)> _handler;
public: //construction
    tcp_server(short port, decltype(_handler) handler);
public:
    void main_loop();
};

}}
